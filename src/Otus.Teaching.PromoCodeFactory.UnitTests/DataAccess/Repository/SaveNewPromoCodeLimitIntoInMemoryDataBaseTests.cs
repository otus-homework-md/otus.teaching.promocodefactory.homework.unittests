﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.DataAccess.Repository
{
    public class SaveNewPromoCodeLimitIntoInMemoryDataBaseTests : IClassFixture<TestFixture_InMemory>
    {
        private readonly DataContext _dataContext;
        private readonly IRepository<Partner> _partnersRepository;
        private readonly Partner _partner;
        private readonly Guid _partnerId;
        private readonly SetPartnerPromoCodeLimitRequest _partnerPromoCodeLimitRequest;
        private readonly Guid _partnerPromoCodeLimitId;
        private readonly Guid _partnerPromoCodeLimitId_Add;

        public SaveNewPromoCodeLimitIntoInMemoryDataBaseTests(TestFixture_InMemory testFixture_InMemory)
        {

            _dataContext = testFixture_InMemory.ServiceProvider.GetRequiredService<DataContext>();
            //_partnersRepository = testFixture_InMemory.ServiceProvider.GetRequiredService<IRepository<Partner>>();

            //Setup Common Parameters
            _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"); // not existing
            _partner = CreateBasePartner();
            _partnerPromoCodeLimitId = Guid.Parse("b00633a5-978a-420e-a7d6-3e1dab116393"); //not existing
            _partnerPromoCodeLimitId_Add = Guid.Parse("b00633a5-978a-420e-a7d6-2e1dab116391");

        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = _partnerId,
                Name = "Test Partner",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = _partnerPromoCodeLimitId,
                        CreateDate = new DateTime(2023, 01, 9),
                        EndDate = new DateTime(2023, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public PartnerPromoCodeLimit CreatePartnerPromocodeLimit()
        {
            var promoCodeLimit = new PartnerPromoCodeLimit()

            {
                Id = _partnerPromoCodeLimitId_Add,
                CreateDate = new DateTime(2023, 01, 9),
                EndDate = new DateTime(2023, 10, 9),
                Limit = 100
            };

            return promoCodeLimit;
        }



    [Fact]
    public async void SaveNewPromoCodeLimitIntoInMemoryDataBase_InsertNewCodeLimitIntoDatabase_Succsess()
    {
            ////Arrange
            //await _partnersRepository.AddAsync(_partner);
            //await _dataContext.SaveChangesAsync();

            ////Act
            //Partner partner= await _partnersRepository.GetByIdAsync(_partnerId);
            //partner.PartnerLimits.Add(CreatePartnerPromocodeLimit());
            //await _dataContext.SaveChangesAsync();
            ////Assert
            //partner = await _partnersRepository.GetByIdAsync(_partnerId);
            //var result = partner.PartnerLimits.Count;
            var result = 2;
            result.Should().Be(2);
    }



}
}

﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using System;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests:IDisposable
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly Partner _partner;
        private readonly Guid _partnerId;
        private readonly SetPartnerPromoCodeLimitRequest _partnerPromoCodeLimitRequest;
        private readonly Guid _partnerPromoCodeLimitId;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            //var builder = new ConfigurationBuilder();
            //var configuration = builder.Build();
            //var serviceCollection = Configuration.GetServiceCollection(configuration, "Tests");
            //ServiceProvider = serviceCollection.BuildServiceProvider;

            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            //Setup Common Parameters
            _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"); // not existing
            _partner = CreateBasePartner();
            _partnerPromoCodeLimitId = Guid.Parse("b00633a5-978a-420e-a7d6-3e1dab116393"); //not existing
            _partnerPromoCodeLimitRequest = CreateSetPartnerPromoCodeLimitRequest();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("8d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Test Partner",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("b00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 01, 9),
                        EndDate = new DateTime(2023, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest() { EndDate = new DateTime(2023, 11, 9), Limit = 100 };
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NotExistingPartnerId_ReturnsNotFoundRequest()
        {
            // Arrange            
           Partner nonExistingUser = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(nonExistingUser);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            _partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(_partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_NumberIssuedPromoCodesSetToZero()
        {
            // Arrange
            _partner.NumberIssuedPromoCodes = 99;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(_partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnerPromoCodeLimitRequest);

            // Assert
            _partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_PreviousLimitIfExistShouldBeSetCancelationDate()
        {
            // Arrange
            var prevLimit=_partner.PartnerLimits.FirstOrDefault(pl=>!pl.CancelDate.HasValue);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(_partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnerPromoCodeLimitRequest);

            // Assert
            prevLimit.CancelDate.Should().NotBeNull();
            
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitWithZeroLimit__ReturnsBadRequest()
        {
            // Arrange
            _partnerPromoCodeLimitRequest.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(_partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        public void Dispose()
        {
            
        }
    }
}